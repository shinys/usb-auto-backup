# README #

프로그램을 C:\usbbackup 에 위치 시키고 C:\usb_backup 폴더에 백업을 수행한다고 가정.
(상기 경로는 아래 설명할 설정파일로 조정 가능.)


1. 실행 배치 파일 수정.
   service_install_(64|32).bat 파일을 편집기로 열어
   AGENT_PATH 에 프로그램파일 경로 C:/usbbackup 를 기술.

   service_uninstall_(64|32).bat 파일 역시 동일하게 AGENT_PATH 기술.


2. C:\usbbackup\conf\application.properties 수정.
   copy.target.dir=C:\\usb_backup\\  로 usb 파일이 백업될 최상위 경로를 지정.

   alarm.sound=on , alarm.sound=off 로 복사 작업 완료 후 알림음 출력 여부를 설정.


3. 윈도우 서비스 등록.
   service_install_(64|32).bat 을 마우스 오른쪽 클릭을 통하여 관리자 권한으로 실행.


4. 윈도우 서비스 제거.
   service_uninstall_(64|32).bat 을 마우스 오른쪽 클릭을 통하여 관리자 권한으로 실행.


5. 서비스 정지
   service_stop.bat 을 마우스 오른쪽 클릭을 통하여 관리자 권한으로 실행.


6. 서비스 시작
   service_start.bat 을 마우스 오른쪽 클릭을 통하여 관리자 권한으로 실행.



기타

1. 복사 수행 이력은 
   C:\usbbackup\log 아래의 로깅 파일로 확인 가능.

2. 복사 완료 알림음 변경은 
   C:\usbbackup\conf\glass_ping.wav 을 동일한 이름의 다른 wav 파일로 교체.
   파일 교체 후 서비스 재시작 필요.

3. 여러 이유로 윈도우즈 서비스 등록에 실패하는 경우
   코멘드 프롬프트(CMD) 를 통해 다음과 같이 Excutable jar를 바로 실행할 수도 있음.
   cd C:\usbbackup
   C:\usbbackup\jre\bin\java -Xmx32M -jar C:\usbbackup\USBCopy-1.0.jar
   

C:\usbbackup\jre\bin\java -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false -Dcom.sun.management.jmxremote=true -Dcom.sun.management.jmxremote.port=9983 -Xmx32M -server -jar USBCopy-1.0.jar