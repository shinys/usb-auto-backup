@echo off

rem *******************************************************
rem * 아래 내용은 사용자 환경에 맞추어 설정하여야 한다.
rem * JAVA_SERVICE 	: JavaService 실행 파일위치 
rem * SERVICE_NAME	: 서비스 이름
rem ******************************************************

set AGENT_PATH=C:/usbbackup


set JAVA_SERVICE="%AGENT_PATH%/JavaService64.exe"
set SERVICE_NAME="USB_Backup"

net stop %SERVICE_NAME%

timeout 3 > nul

%JAVA_SERVICE% -uninstall %SERVICE_NAME%
pause

