@echo off

rem *******************************************************
rem * 아래 내용은 사용자 환경에 맞추어 설정하여야 한다.
rem * JAVA_HOME 	: java설치된 경로
rem * JAVA_SERVICE 	: JavaService 실행 파일위치 
rem * SERVICE_NAME	: 서비스 이름
rem * AGENT_PATH	: 프로그램 경로
rem *******************************************************

set AGENT_PATH=C:/usbbackup
set SERVICE_NAME="USB_Backup"


rem *******************************************************
rem * 아래 내용은 수정 하지 마시오.
rem *******************************************************

set JAVA_HOME=%AGENT_PATH%/jre
set JAVA_SERVICE=%AGENT_PATH%/JavaService64.exe
set CLASSPATH=%JAVA_HOME%/lib/rt.jar
set CLASSPATH=%AGENT_PATH%/lib/h2-1.3.176.jar
set CLASSPATH=%AGENT_PATH%/lib/log4j-1.2.17.jar;%CLASSPATH%
set CLASSPATH=%AGENT_PATH%/lib/slf4j-api-1.7.12.jar;%CLASSPATH%
set CLASSPATH=%AGENT_PATH%/lib/slf4j-log4j12-1.7.12.jar;%CLASSPATH%
set CLASSPATH=%AGENT_PATH%/USBCopy-1.0.jar;%CLASSPATH%
set CLASSPATH=%AGENT_PATH%/conf;%CLASSPATH%
set CLASSPATH=%AGENT_PATH%/log;%CLASSPATH%

%JAVA_SERVICE% -install %SERVICE_NAME%  %JAVA_HOME%/bin/server/jvm.dll -Djava.class.path=%CLASSPATH% -Xmx32M -start com.usbcopy.AppMain -out %AGENT_PATH%/log/javaservice_out.log -err %AGENT_PATH%/log/javaservice_err.log -current %AGENT_PATH% -description "USB 자동 백업 프로그램"

timeout 3 > nul

net start %SERVICE_NAME%

pause
