package com.usbcopy;

import com.usbcopy.repository.DBProcess;
import com.usbcopy.task.CopyTask;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.nio.file.FileVisitOption;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.filechooser.FileSystemView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author shin younseob <yunsobi at gmail dot com>
 */
public class AppMain {

    private static final Logger logger = LoggerFactory.getLogger(AppMain.class);
    private static Properties configProps = null;
    protected static final Map<String, Thread> threadPool = new HashMap<String, Thread>(5);

    public AppMain() {
        AppMain.configProps = new Properties();
        File confFile = new File("conf/application.properties");
        InputStream confInputStream = null;
        try {
            logger.info("Load Properties from  " + confFile.getAbsolutePath());
            confInputStream = new FileInputStream(confFile);
            AppMain.configProps.load(confInputStream);

            //초기 디렉토리 확인
            File dist = new File(configProps.getProperty("copy.target.dir", "C:\\USB_bakups\\"));
            if (!dist.exists()) {
                dist.mkdirs();
            }

        } catch (IOException ex) {
            logger.error("conf 폴더내에 application.properties 파일이 존재하지 않습니다.", ex);
            System.exit(-1);
        } finally {
            if (confInputStream != null) {
                try {
                    confInputStream.close();
                } catch (IOException ex) {
                }
            }
        }
    }

    public static void main(String[] args) throws URISyntaxException {
        AppMain s = new AppMain();

        //System.out.println(AppMain.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath());
        //System.out.println(ClassLoader.getSystemClassLoader().getResource(".").getPath());
        //shutdownhook 생성
        ShutdownHook hook = new ShutdownHook(s);
        Thread hookThread = new Thread(hook);
        hookThread.setDaemon(false);

        //shutdownhook 등록
        Runtime.getRuntime().addShutdownHook(hookThread);
        s.execute();

    }

    Clip clip = null;
    void execute() {
        
        //사운드 클립 준비
        try {
            String jarPath = AppMain.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath();
            String soundPath = jarPath.substring(1, jarPath.lastIndexOf("/")) + "/conf/glass_ping.wav";
            //Open an audio input stream.
            //URL url = this.getClass().getClassLoader().getResource("glass_ping-Go445-1207030150.wav");
            //AudioInputStream audioIn = AudioSystem.getAudioInputStream(url);
            AudioInputStream audioIn = AudioSystem.getAudioInputStream(new File(soundPath));
            // Get a sound clip resource.
            clip = AudioSystem.getClip();
            // Open audio clip and load samples from the audio input stream.
            clip.open(audioIn);
        } catch(IllegalArgumentException ex){
            logger.error("사운드 출력 불가!",ex);
        }catch (URISyntaxException ex) {
            logger.error(null, ex);
        } catch (UnsupportedAudioFileException e) {
            logger.error(null, e);
        } catch (IOException e) {
            logger.error(null, e);
        } catch (LineUnavailableException e) {
            logger.error(null, e);
        }
        
        DBProcess db = new DBProcess(configProps);

        //timertask
        TimerTask driveFinderTask = new TimerTask() {
            //private final ThreadLocal<FileSystemWatcher> TL = new ThreadLocal<FileSystemWatcher>();
            //ThreadGroup tg = new ThreadGroup("FileSystemWatcherGroup");
            @Override
            public void run() {
                String drive;                // 드라이브 명
                double size;                 // 드라이브의 최대 크기 = 용량
                String name;

                NumberFormat nf = NumberFormat.getInstance();
                nf.setMaximumFractionDigits(2);                                   // 소숫점 2자리까지만 보이게 변환
                
                List<Map> savedRemovableDrives = null;
                try {
                    savedRemovableDrives = db.selectDatas();
                } catch (SQLException ex) {
                    logger.error(null, ex);
                }

                File[] roots = File.listRoots();
                for (File root : roots) {

                    drive = root.getAbsolutePath();
                    name = root.getName();
                    size = root.getTotalSpace() / Math.pow(1024, 3);

                    String letter = FileSystemView.getFileSystemView().getSystemDisplayName(root).replaceAll(":", "");
                    String type = getDriveType(drive.substring(0, drive.indexOf(":")));

//                    System.out.println("Drive : " + drive);
//                    System.out.println("letter : " + letter);
//                    System.out.println("Space : " + nf.format(size) + " GB");
//                    System.out.println("size : " + root.getTotalSpace());                    
//                    System.out.println("Type : " + type);
                    //이동식 디스크 타입
                    if ("2".equals(type.trim()) &&  (  size > 0.1D && size < 130.0D )  ) {
                        //savedRemovableDrives 에서 꽂혀있는 디스크 제외하기.
                        for(Map data: savedRemovableDrives){
                            if( drive.equals((String)data.get("DRIVE"))  && root.getTotalSpace() == (long)data.get("SIZE") ){
                                savedRemovableDrives.remove(data);
                                break;
                            }
                        }                       
                        
                        try {
                            //watcher 등록  
                            // 이번 의뢰 작업에 FileSystemWatcher 까지는 필요 없음.
                            // 왓처 필요 시 아래 주석 해제
//                            Thread t = threadPool.get(drive);
//                            if (t != null) {
//                                t.interrupt();
//                                threadPool.remove(drive);
//                            }
//                            Thread fwt = new Thread(new FileSystemWatcher(copyFrom, copyTo, true, new FileNotifier()));
//                            fwt.setDaemon(true);
//                            fwt.setPriority(Thread.MIN_PRIORITY);
//                            fwt.setUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
//                                @Override
//                                public void uncaughtException(Thread t, Throwable e) {
//                                    logger.error("UncaughtException FileSystemWatcher", e);
//                                }
//                            });
//                            fwt.start();
//                            threadPool.put(drive, fwt);
                            
                            //DB상의 drive 정보 확인. 드라이브 명, 사이즈
                            Map result = db.selectLastestData(drive);
//                            if(result!=null){
//                                logger.info(result.toString());
//                                System.out.println(result.toString());
//                            }
                            //System.out.println(result);
                            //if (result == null || (System.currentTimeMillis() - ((java.sql.Timestamp) result.get("LASTCOPY")).getTime()) > 1000 * 60 * 30 || (long) result.get("SIZE") != root.getTotalSpace()) //전체 복사 시작
                            //if (result == null || result.isEmpty() || (long) result.get("SIZE") != root.getTotalSpace()) //새로 꽂혔다고 판단되는 경우 전체 복사 시작
                            if (result == null || result.isEmpty() ) //새로 꽂혔다고 판단되는 경우 전체 복사 시작
                            {
                                logger.info("+++ Drive : " + drive+", letter : " + letter+", Space : " + nf.format(size) + " GB  연결 감지.");
                                System.out.println("+++ Drive : " + drive+", letter : " + letter+", Space : " + nf.format(size) + " GB  연결 감지.");
//                                logger.info("letter : " + letter);
//                                logger.info("Space : " + nf.format(size) + " GB");
//                                logger.info("size : " + root.getTotalSpace());
//                                logger.info("Type : " + type);
                                
                           
                                DateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
                                //logger.info(drive + "를 " + configProps.getProperty("copy.target.dir", "C:\\USB_bakups\\") + letter + "로 복사 시작");
                                Path copyFrom = Paths.get(drive);
                                Path copyTo = Paths.get(configProps.getProperty("copy.target.dir", "C:\\USB_bakups\\") + df.format(new Date()) + "\\" + letter);
                                File dist = new File(configProps.getProperty("copy.target.dir", "C:\\USB_bakups\\") + df.format(new Date()) + "\\" + letter);
                                if (!dist.exists()) {
                                    dist.mkdirs();
                                }

                                CopyTask visitor = new CopyTask(copyFrom, copyTo);
                                EnumSet options = EnumSet.of(FileVisitOption.FOLLOW_LINKS);

                                Files.walkFileTree(copyFrom, options, Integer.MAX_VALUE, visitor);

                                if (result == null || result.isEmpty()) {
                                    //insert
                                    db.insertLastestData(drive, letter, root.getTotalSpace());
                                } else {
                                    //update
                                    db.updateLastesData(drive, letter, root.getTotalSpace());
                                }

                                if (clip!=null && "on".equalsIgnoreCase(configProps.getProperty("alarm.sound", "").trim())) {
                                    if (clip.isRunning())
                                        clip.stop();   // Stop the player if it is still running
                                    clip.setFramePosition(0);
                                    clip.start();
                                }else{
                                    logger.info( "sound off");
                                }
                                
                            }
                        }catch(NoSuchFileException ex){
                            logger.error("복사 중에 드라이브 강제 제거 됨.", ex);
                            System.out.println("+++ 복사 중에 드라이브 강제 제거 됨.");
                        } catch (IOException ex) {
                            logger.error(null, ex);
                        } catch (ClassNotFoundException ex) {
                            logger.error(null, ex);
                            System.exit(-1);
                        } catch (SQLException ex) {
                            logger.error(null, ex);
                        }

                    } else if ("2".equals(type.trim()) && size > 0.1D) {
                        logger.warn(drive + " " + letter + "는 이동식 드라이브이지만 " + nf.format(size) + " GB 이므로 복사를 수행하지 않음.");
                        System.out.println(drive + " " + letter + "는 이동식 드라이브이지만 " + nf.format(size) + " GB 이므로 복사를 수행하지 않음.");
                    }

                }
                
                //savedRemovableDrives에 남아있는 항목은 뽑아버린 드라이브이므로 삭제
                for(Map data: savedRemovableDrives){
                    try {                        
                        db.deleteData((String)data.get("DRIVE"));
                        logger.info("+++ "+(String)data.get("DRIVE")+" "+ (String)data.get("LETTER") +"는 제거되었음.");
                        System.out.println("+++ "+(String)data.get("DRIVE")+" "+ (String)data.get("LETTER") +"는 제거되었음.");
                        //System.out.println((String)data.get("DRIVE")+" "+ (String)data.get("LETTER") +"는 제거되었음.");
                    } catch (SQLException ex) {
                        logger.error(null,ex);
                    }
                }
            }
        };

        Timer timer = new Timer(false);
        timer.schedule(driveFinderTask, 1000, 2000);
    }

    private String getDriveType(String drive) {
        String result = "";
        File file = null;
        try {
            file = File.createTempFile("drivetype_" + drive, ".vbs");
            //System.out.println(file.getAbsolutePath());
            //if(!file.exists() || file.length() < 1){
            file.deleteOnExit();
            FileWriter fw = new java.io.FileWriter(file);
            String vbs
                    = "Set objWMIService = GetObject(\"winmgmts:\")\n"
                    + "Set objDisk = objWMIService.Get(\"Win32_LogicalDisk.DeviceID=\'"
                    + drive
                    + ":\'\")\n" + "Wscript.Echo objDisk.DriveType";
            fw.write(vbs);
            fw.close();
            //}

            Process p = Runtime.getRuntime().exec("cscript //NoLogo " + file.getPath());
            BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));

            String line;
            while ((line = input.readLine()) != null) {
                result += line;
            }

            input.close();
            p.destroyForcibly();
            
        } catch (Exception e) {
            logger.error("드라이브 타입 체크 VBScript 처리 중 오류", e);
        }finally{
            if(file!=null){
                file.delete();
            }
        }
        return result;
    }

    class FileNotifier {
        void nofify(Path orgnFile, Path copyFrom, Path copyTo) {
            try {
                Files.copy(orgnFile, copyTo.resolve(copyFrom.relativize(orgnFile)), StandardCopyOption.REPLACE_EXISTING, StandardCopyOption.COPY_ATTRIBUTES);
                logger.info("copy from " + orgnFile + " \t\t to \t" + copyTo.resolve(copyFrom.relativize(orgnFile)));
                //System.out.println("copy from " + orgnFile + " to " + copyTo.resolve(copyFrom.relativize(orgnFile)));
            } catch (IOException e) {
                logger.error(null, e);
            }
        }
    }
}
