package com.usbcopy;

import java.util.Iterator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author  shin younseob <yunsobi at gmail dot com>
 */
public class ShutdownHook implements Runnable {

    private static final Logger logger = LoggerFactory.getLogger(ShutdownHook.class);
    private AppMain server = null;

    public ShutdownHook(AppMain server) {
        this.server = server;
    }

    @Override
    public void run() {
        logger.warn("Now USBCOPY shutdown... please wait");
        Iterator<String> iter = AppMain.threadPool.keySet().iterator();
        while( iter.hasNext() ){
            String key = iter.next();
            try{
                AppMain.threadPool.get(key).interrupt();            
            }catch(SecurityException se){
                
            }
        }
        AppMain.threadPool.clear();
    }
    
}
