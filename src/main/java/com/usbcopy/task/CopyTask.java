package com.usbcopy.task;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryNotEmptyException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author  shin younseob <yunsobi at gmail dot com>
 */
public class CopyTask implements FileVisitor {
    private static final Logger logger = LoggerFactory.getLogger(CopyTask.class);
    Path copyFrom = null;
    Path copyTo = null;

    public CopyTask(String sourceDrive, String targetPath) {

        File dist = new File(targetPath);
        if (!dist.exists()) {
            dist.mkdirs();
        }

        copyFrom = Paths.get(sourceDrive);
        copyTo = Paths.get(targetPath);

    }

    public CopyTask(Path sourceDrive, Path targetPath) {

        copyFrom = sourceDrive;
        copyTo = targetPath;
    }

    static void copySubTree(Path copyFrom, Path copyTo) {
        try {
            Files.copy(copyFrom, copyTo, StandardCopyOption.REPLACE_EXISTING, StandardCopyOption.COPY_ATTRIBUTES);
        } catch (IOException e) {
            logger.error(null,e);
        }
    }

    @Override
    public FileVisitResult preVisitDirectory(Object dir, BasicFileAttributes attrs) throws IOException {
        Path newDir = copyTo.resolve(copyFrom.relativize((Path) dir));
        try {
            Files.copy((Path) dir, newDir, StandardCopyOption.COPY_ATTRIBUTES);
        }catch(FileAlreadyExistsException e){
            //이미 존재하는 디렉토리
        }catch(DirectoryNotEmptyException de){
            //do nothing
        }
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFile(Object file, BasicFileAttributes attrs) throws IOException {        
        copySubTree((Path) file, copyTo.resolve(copyFrom.relativize((Path) file)));
        logger.info("copy from "+ (Path)file +" \t\t to \t"+ copyTo.resolve(copyFrom.relativize((Path) file)));
        //System.out.println("copy from "+ (Path)file +" to "+ copyTo.resolve(copyFrom.relativize((Path) file)));
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFileFailed(Object file, IOException exc) throws IOException {
        if(exc != null ){
            throw exc;
        }
        return FileVisitResult.CONTINUE;        
    }

    @Override
    public FileVisitResult postVisitDirectory(Object dir, IOException exc) throws IOException {
        if (exc == null) {
            Path newPath = copyTo.resolve(copyFrom.relativize((Path) dir));

            try {
                FileTime time = Files.getLastModifiedTime((Path) dir);
                Files.setLastModifiedTime(newPath, time);
            } catch (IOException e) {
                logger.error(null,e);
            }

        } else {
            throw exc;
        }
        return FileVisitResult.CONTINUE;
    }

}
