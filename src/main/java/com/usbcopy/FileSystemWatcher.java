package com.usbcopy;

import com.usbcopy.AppMain.FileNotifier;
import java.nio.file.*;
import static java.nio.file.StandardWatchEventKinds.*;
import static java.nio.file.LinkOption.*;
import java.nio.file.attribute.*;
import java.io.*;
import java.util.*;
import org.slf4j.LoggerFactory;

/**
 *
 * @author shin younseob <yunsobi at gmail dot com>
 */
public class FileSystemWatcher implements Runnable {

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(FileSystemWatcher.class);
    private final WatchService watcher;
    private final Map<WatchKey, Path> keys;
    private final boolean recursive;
    private boolean trace = false;
    private final FileNotifier notifier;
    private final Path toDir;
    private final Path fromDir;

    @SuppressWarnings("unchecked")
    static <T> WatchEvent<T> cast(WatchEvent<?> event) {
        return (WatchEvent<T>) event;
    }

    /**
     * Register the given directory with the WatchService
     */
    private void register(Path dir) throws IOException {
        WatchKey key = dir.register(watcher, ENTRY_CREATE, ENTRY_MODIFY);
        if (trace) {
            Path prev = keys.get(key);
            if (prev == null) {
                System.out.format("register: %s\n", dir);
            } else {
                if (!dir.equals(prev)) {
                    System.out.format("update: %s -> %s\n", prev, dir);
                }
            }
        }
        keys.put(key, dir);
    }

    /**
     * Register the given directory, and all its sub-directories, with the
     * WatchService.
     */
    private void registerAll(final Path start) throws IOException {
        // register directory and sub-directories
        Files.walkFileTree(start, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs)
                    throws IOException {
                register(dir);
                return FileVisitResult.CONTINUE;
            }
        });
    }

    /**
     * Creates a WatchService and registers the given directory
     */
    FileSystemWatcher(Path fromDir, Path toDir, boolean recursive, FileNotifier notifier) throws IOException {
        this.watcher = FileSystems.getDefault().newWatchService();
        this.keys = new HashMap<WatchKey, Path>();
        this.recursive = recursive;
        this.notifier = notifier;
        this.toDir = toDir;
        this.fromDir = fromDir;

        if (recursive) {
            //System.out.format("Scanning %s ...\n", dir);
            registerAll(fromDir);
            //System.out.println("Done.");
        } else {
            register(fromDir);
        }

        // enable trace after initial registration
        this.trace = true;
    }

    /**
     * Process all events for keys queued to the watcher
     */
    @Override
    public void run() {
        try {
            while (!Thread.currentThread().isInterrupted()) {

                // wait for key to be signalled
                WatchKey key;
                try {
                    key = watcher.take();
                } catch (InterruptedException x) {
                    return;
                }

                Path dir = keys.get(key);
                if (dir == null) {
                    System.err.println("WatchKey not recognized!!");
                    continue;
                }

                for (WatchEvent<?> event : key.pollEvents()) {
                    WatchEvent.Kind kind = event.kind();

                    // TBD - provide example of how OVERFLOW event is handled
                    if (kind == OVERFLOW) {
                        continue;
                    }

                    // Context for directory entry event is the file name of entry
                    WatchEvent<Path> ev = cast(event);
                    Path name = ev.context();
                    Path child = dir.resolve(name);

                    // print out event
                    System.out.format("%s: %s\n", event.kind().name(), child);
                    if (kind == ENTRY_MODIFY && !Files.isDirectory(child, NOFOLLOW_LINKS) && this.notifier != null) {
                        //noti for file modified
                        this.notifier.nofify(child, fromDir, toDir);
                    }

                    // if directory is created, and watching recursively, then
                    // register it and its sub-directories
                    if (recursive && (kind == ENTRY_CREATE)) {
                        try {
                            if (Files.isDirectory(child, NOFOLLOW_LINKS)) {
                                registerAll(child);
                            }
                        } catch (IOException x) {
                            // ignore to keep sample readbale
                        }
                    }
                }

                // reset key and remove from set if directory no longer accessible
                boolean valid = key.reset();
                if (!valid) {
                    keys.remove(key);
                    // all directories are inaccessible
                    if (keys.isEmpty()) {
                        break;
                    }
                }

            }
            
        } catch (Exception e) {
            logger.error(null, e);
        }finally{
            stop();
        }
    }

    void stop() {
        if (this.watcher != null) {
            try {
                this.watcher.close();
            } catch (IOException ex) {
                logger.error(null, ex);
            }
        }
    }

//    static void usage() {
//        System.err.println("usage: java WatchDir [-r] dir");
//        System.exit(-1);
//    }
//
//    public static void main(String[] args) throws IOException {
//        // parse arguments
////        if (args.length == 0 || args.length > 2)
////            usage();
////        boolean recursive = false;
////        int dirArg = 0;
////        if (args[0].equals("-r")) {
////            if (args.length < 2)
////                usage();
////            recursive = true;
////            dirArg++;
////        }
////
////        // register directory and process its events
////        Path dir = Paths.get(args[dirArg]);
//        Path dir = Paths.get("D:\\var");
//        new FileSystemWatcher(dir, true , null).processEvents();
//    }
}
