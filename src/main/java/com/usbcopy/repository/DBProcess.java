package com.usbcopy.repository;

import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author shin younseob <yunsobi at gmail dot com>
 */
public class DBProcess {

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(DBProcess.class);
    Properties props = null;
    String rootPath = null;

    public DBProcess(Properties props) {
        this.props = props;

        String jarPath;
        try {
            // result like this : /C:/project/USBCopy/target/USBCopy-1.0.jar
            jarPath = DBProcess.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath();
            rootPath = jarPath.substring(1, jarPath.lastIndexOf("/")) + "/conf";
        } catch (URISyntaxException ex) {
            logger.error(null, ex);
            rootPath = "~";
        }

    }

    private Connection getConnection() throws ClassNotFoundException, SQLException {
        Class.forName("org.h2.Driver");
        Connection conn = DriverManager.getConnection(
                //String.format(  "jdbc:h2:file:%s/uc;IFEXISTS=TRUE;INIT=RUNSCRIPT FROM '%s/h2.init.sql'\\;DB_CLOSE_ON_EXIT=TRUE" , rootPath, rootPath)  
                String.format("jdbc:h2:file:%s/uc;IFEXISTS=TRUE;DB_CLOSE_ON_EXIT=TRUE", rootPath), "sa", ""
        );
        return conn;
    }

    public Map selectLastestData(String drive) throws ClassNotFoundException, SQLException {

        Connection con = getConnection();
        PreparedStatement pstmt = con.prepareStatement(
                "SELECT * FROM COPYDATA WHERE DRIVE=?;", 
                ResultSet.TYPE_FORWARD_ONLY, 
                ResultSet.CONCUR_READ_ONLY, 
                ResultSet.CLOSE_CURSORS_AT_COMMIT
        );
        pstmt.setString(1, drive);
        ResultSet rs = pstmt.executeQuery();

        Map result = null;
        if (rs.next()) {
            result = new HashMap(1);
            result.put("DRIVE", rs.getString("DRIVE"));
            result.put("LETTER", rs.getString("LETTER"));
            result.put("SIZE", rs.getLong("SIZE"));
            result.put("LASTCOPY", rs.getTimestamp("LASTCOPY"));
        }
        rs.close();
        pstmt.close();
        con.close();

        return result;
    }

    public List<Map> selectDatas() throws SQLException {
        List<Map> list = new ArrayList<Map>(3);
        Connection con;
        try {
            con = getConnection();

            PreparedStatement pstmt = con.prepareStatement(
                    "SELECT * FROM COPYDATA;", 
                    ResultSet.TYPE_FORWARD_ONLY, 
                    ResultSet.CONCUR_READ_ONLY, 
                    ResultSet.CLOSE_CURSORS_AT_COMMIT
            );

            ResultSet rs = pstmt.executeQuery();

            //Map result = null;
            while (rs.next()) {
                Map result = new HashMap(1);
                result.put("DRIVE", rs.getString("DRIVE"));
                result.put("LETTER", rs.getString("LETTER"));
                result.put("SIZE", rs.getLong("SIZE"));
                result.put("LASTCOPY", rs.getTimestamp("LASTCOPY"));
                list.add(result);
            }
            rs.close();
            pstmt.close();
            con.close();

        } catch (ClassNotFoundException ex) {
            logger.error(null,ex);
        }

        return list;
    }

    public void deleteData(String drive) throws SQLException {

        try {
            Connection con = getConnection();
            PreparedStatement pstmt = con.prepareStatement(
                    "DELETE FROM COPYDATA WHERE DRIVE=?;",
                    ResultSet.TYPE_FORWARD_ONLY,
                    ResultSet.CONCUR_READ_ONLY,
                    ResultSet.CLOSE_CURSORS_AT_COMMIT
            );
            
            pstmt.setString(1, drive);
            pstmt.execute();
            
            pstmt.close();
            con.close();
        } catch (ClassNotFoundException ex) {
            logger.error(null,ex);
        }
    }

    public void insertLastestData(String drive, String letter, long size) throws ClassNotFoundException, SQLException {
        Connection con = getConnection();
        PreparedStatement pstmt = con.prepareStatement(
                "INSERT INTO COPYDATA(DRIVE,LETTER,SIZE,LASTCOPY) VALUES (?,?,?, CURRENT_TIMESTAMP());", 
                ResultSet.TYPE_FORWARD_ONLY, 
                ResultSet.CONCUR_READ_ONLY, 
                ResultSet.CLOSE_CURSORS_AT_COMMIT
        );
        pstmt.setString(1, drive);
        pstmt.setString(2, letter);
        pstmt.setLong(3, size);
        pstmt.execute();

        pstmt.close();
        con.close();
    }

    public void updateLastesData(String drive, String letter, long size) throws ClassNotFoundException, SQLException {
        Connection con = getConnection();
        PreparedStatement pstmt = con.prepareStatement(
                "UPDATE COPYDATA SET LETTER=? ,SIZE=? ,LASTCOPY= CURRENT_TIMESTAMP() WHERE DRIVE=?;", 
                ResultSet.TYPE_FORWARD_ONLY, 
                ResultSet.CONCUR_READ_ONLY, 
                ResultSet.CLOSE_CURSORS_AT_COMMIT
        );

        pstmt.setString(1, letter);
        pstmt.setLong(2, size);
        pstmt.setString(3, drive);
        pstmt.execute();

        pstmt.close();
        con.close();
    }

}
